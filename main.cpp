#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include "hash_list.h"


using namespace std;


int main() {
	int size;
	cin >> size;
	
	list_record* tab = create_tab(size);

	char key[256];
	int value;
	for (int i = 0; i < size; i++) {
		scanf("%s %d", key, &value);
		add_record(tab, key, value, size);
	}

	char to_search[256];
	cin.getline(to_search, 255);

	cout << tab[search_record(tab, to_search, size)].value;


	return 0;
}
#include "hash_list.h"


const unsigned long long int P = 65521;

list_record* create_tab(int size){
	size = size * 2;
	list_record* tab_ptr = (list_record*)calloc(size, sizeof(list_record));
	return tab_ptr;
}

unsigned short char_key_toNumeric(char* key) {
	unsigned short numeric_key = 0;

	Word word;
	word.word[0] = 0;
	word.word[1] = 0;

	int in_key_counter = 0;

	while (true) {
		if (key[in_key_counter] != 0) {
			if (in_key_counter % 2 != 0) {

				word.word[0] = key[in_key_counter];
				in_key_counter++;

			}
			else if (in_key_counter % 2 == 0) {
				word.word[1] = key[in_key_counter];

				if (numeric_key >= numeric_key + ((unsigned short)word.word[0] << 8) + (unsigned short)word.word[1]) {
					numeric_key = ((numeric_key + ((unsigned short)word.word[0] << 8) + (unsigned short)word.word[1]) << 1) + 1;
				}
				else {
					numeric_key = numeric_key + ((unsigned short)word.word[0] <<  8) + (unsigned short)word.word[1];
				}

				word.word[0] = 0;
				word.word[1] = 0;

				in_key_counter++;

			}
		}
		else {
			if (in_key_counter % 2 != 0) {
				if (numeric_key >= numeric_key + ((unsigned short)word.word[0] << 8) + (unsigned short)word.word[1]) {
					numeric_key = ((numeric_key + ((unsigned short)word.word[0] << 8) + (unsigned short)word.word[1]) << 1) + 1;
				}
				else {
					numeric_key = numeric_key + ((unsigned short)word.word[0] << 8) + (unsigned short)word.word[1];
				}
			}

			break;
		}
	}

	return numeric_key;
}

unsigned int multiplicationHash(int k, unsigned int N)
{
	return ((P * k) % UINT_MAX) * N >> 32;
}

unsigned int algWilliams(list_record* array, unsigned int mindex, unsigned int N)
{
	unsigned int R = N;
	if (array[mindex].taken)
	{
		R--;
		while (array[R].taken) {
			R--;
		}
		int i = mindex;
		while (array[i].next_index != -1) {
			i = array[i].next_index;

		}
		array[i].next_index = R;
		return R;
	}
	else
	{
		return mindex;
	}
}

void add_record(list_record* tab, char* key, int value, int size) {
	unsigned int index;
	index = algWilliams(tab, multiplicationHash(char_key_toNumeric(key), size*2), size*2);
	strcpy(tab[index].key, key);
	tab[index].value = value;
	tab[index].taken = true;
}

unsigned int search_record(list_record* tab, char* key, int size) {
	unsigned int index;
	index = algWilliams(tab, multiplicationHash(char_key_toNumeric(key), size*2), size*2);
	return index;
}
